// Barak Gonen 2019
// Skeleton code - inject DLL to a running process
#include <windows.h>
#include <fileapi.h>
#include <iostream>
#include <tlhelp32.h>
#include <string>
#include <psapi.h> 
#define LIBRARY "C:\\Users\\user\\source\\repos\\DLL project\\x64\\Debug\\static library.dll"
#define PATH_LEN 100

//returns 1 if fails, 0 otherwise
int getProcId(std::wstring targetProcessName)//modified the code from here http://en.ciholas.fr/get-process-id-pid-from-process-name-string-c-windows-api/
{
	HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); //all processes

	PROCESSENTRY32W entry; //current process
	entry.dwSize = sizeof entry;

	if (!Process32FirstW(snap, &entry)) { 
		return 1;
	}

	do {
		if (std::wstring(entry.szExeFile) == targetProcessName) {
			return entry.th32ProcessID; //name matches
		}
	} while (Process32NextW(snap, &entry));
	return 0;
}
void handleError(char * error)
{
	std::cout << "Error number " << GetLastError() << std::endl << error << std::endl;
	system("PAUSE");
}
int main()
{
	char buffer[PATH_LEN] = { 0 };
	TCHAR** lppPart = { NULL };
	// Get full path of DLL to inject
	if (0 == GetFullPathNameA("static library.dll", PATH_LEN, buffer, lppPart))
	{
		handleError("Could not get the name of the static library to launch");
		return 1;
	}
	HMODULE moduleHandle = GetModuleHandle("kernel32.dll");
	if (NULL == moduleHandle) {
		handleError("Could not get module handle");
		return 1;
	}
	PVOID addrLibrary = (PVOID)GetProcAddress(moduleHandle,"LoadLibraryA");
	if (NULL == addrLibrary)
	{
		handleError("could not get the adress of the LoadLibraryA of the kernel32 module");
		return 1;
	}
	DWORD processID = getProcId(L"notepad.exe");
	if (NULL == processID) {
		handleError("Could not get the proccess id of notepad.exe. Make sure it is running.");
		return 1;
	}
	HANDLE 	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID);
	if (NULL == hProcess)
	{
		handleError("could not open the proccess");
		return 1;
	}
	// Get a pointer to memory location in remote process,
	// big enough to store the DLL's path
	PVOID memAddr = (PVOID)VirtualAllocEx(hProcess,NULL, sizeof(buffer), MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (NULL == memAddr) {
		handleError("could not get the memory adress of the proccess");
		return 1;
	}
	// Write DLL name to remote process memory
	BOOL check = WriteProcessMemory(hProcess, memAddr, buffer,strlen(buffer),NULL);
	if (!check) {
		handleError("Could not write the dll to the remote process");
		return 1;
	}

	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	HANDLE hRemote = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)addrLibrary,memAddr, NULL, NULL);
	if (NULL == hRemote) {
		handleError("could not get create the remote thread");
		return 1;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	system("PAUSE");
	return 0;
}